/*
* WoolGame is a bukkit plugin based off of the popular minigame WoolMix up.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.woolgame;

import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;

public class GetArenaLocation {
		
	
	 public static WorldGuardPlugin getWorldGuard() {
		    Plugin plugin = WoolGame.getSelf().getServer().getPluginManager().getPlugin("WorldGuard");
		     
		    if ((plugin == null) || (!(plugin instanceof WorldGuardPlugin))) {
		    	return null;
		    }
		     	return (WorldGuardPlugin)plugin;
		    }
		    
		    public static void getLocation(String arena, World world){
		    	RegionManager rm = getWorldGuard().getRegionManager(world);
		    	
		    	WoolGameArena.minX = rm.getRegion("ba-" + arena).getMinimumPoint().getBlockX();
		    	WoolGameArena.minZ = rm.getRegion("ba-" + arena).getMinimumPoint().getBlockZ();
				WoolGameArena.minY = rm.getRegion("ba-" + arena).getMinimumPoint().getBlockY(); 
				
				WoolGameArena.maxX = rm.getRegion("ba-" + arena).getMaximumPoint().getBlockX();
				WoolGameArena.maxZ = rm.getRegion("ba-" + arena).getMaximumPoint().getBlockZ();
				WoolGameArena.maxY = rm.getRegion("ba-" + arena).getMaximumPoint().getBlockY();
				
				WoolGameArena.world = world;
		   }
}

