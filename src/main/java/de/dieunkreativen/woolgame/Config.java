/*
* WoolGame is a bukkit plugin based off of the popular minigame WoolMix up.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.woolgame;

import java.util.ArrayList;

import org.bukkit.DyeColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class Config {
	static ArrayList<DyeColor> color = new ArrayList<DyeColor>();
	static int delayInTicks = 130;
	
	public void loadValues() {
		Plugin plugin = WoolGame.getSelf();
		
		FileConfiguration config = plugin.getConfig();
		
		delayInTicks = config.getInt("delayInTicks");
		
		if (config.getBoolean("DyeColors.RED")) {
			color.add(DyeColor.RED);
		}
		if (config.getBoolean("DyeColors.WHITE")) {
			color.add(DyeColor.WHITE);
		}
		if (config.getBoolean("DyeColors.BLACK")) {
			color.add(DyeColor.BLACK);
		}
		if (config.getBoolean("DyeColors.LIGHT_BLUE")) {
			color.add(DyeColor.LIGHT_BLUE);
		}
		if (config.getBoolean("DyeColors.ORANGE")) {
			color.add(DyeColor.ORANGE);
		}
		if (config.getBoolean("DyeColors.BROWN")) {
			color.add(DyeColor.BROWN);
		}
		if (config.getBoolean("DyeColors.GREEN")) {
			color.add(DyeColor.GREEN);
		}
		if (config.getBoolean("DyeColors.YELLOW")) {
			color.add(DyeColor.YELLOW);
		}
		if (config.getBoolean("DyeColors.PURPLE")) {
			color.add(DyeColor.PURPLE);
		}
		if (config.getBoolean("DyeColors.BLUE")) {
			color.add(DyeColor.BLUE);
		}
		if (config.getBoolean("DyeColors.LIME")) {
			color.add(DyeColor.LIME);
		}
		if (config.getBoolean("DyeColors.GRAY")) {
			color.add(DyeColor.GRAY);
		}
		if (config.getBoolean("DyeColors.PINK")) {
			color.add(DyeColor.PINK);
		}
		if (config.getBoolean("DyeColors.MAGENTA")) {
			color.add(DyeColor.MAGENTA);
		}
		if (config.getBoolean("DyeColors.SILVER")) {
			color.add(DyeColor.SILVER);
		}
	}
	
	
	
	

}

