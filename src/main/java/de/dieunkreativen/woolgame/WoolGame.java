/*
* WoolGame is a bukkit plugin based off of the popular minigame WoolMix up.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.woolgame;

import java.util.logging.Logger;

import mc.alk.arena.BattleArena;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;


public class WoolGame extends JavaPlugin {
	static WoolGame plugin;
	Logger log;
	Config config = new Config();
	
    @Override
    public void onLoad() {
    	log = getLogger();
    }
	
	@Override
	public void onEnable() {
		plugin = this;
		saveDefaultConfig();
		config.loadValues();
		PluginDescriptionFile pdfFile = this.getDescription();
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
		BattleArena.registerCompetition(this, "WoolGame", "wg", WoolGameArena.class);
		
	}
	@Override
	public void onDisable() {
		log.info("WoolGame is disabled!");
	}

    
	public static WoolGame getSelf() {
		return plugin;
	}
	
    @Override
    public void reloadConfig(){
            super.reloadConfig();
            config.loadValues();
    }

    
   
    	
}
	

	

	



