/*
* WoolGame is a bukkit plugin based off of the popular minigame WoolMix up.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.woolgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.server.v1_10_R1.BlockPosition;
import net.minecraft.server.v1_10_R1.IBlockData;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Wool;

import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.events.ArenaEventHandler;

public class WoolGameArena extends Arena{
	
	public static int minX;
	public static int minY;
	public static int minZ;
	
	public static int maxX;
	public static int maxY;
	public static int maxZ;
	
	public static World world;
	
	int task1, task2, task3, task4, task5, round;
	boolean WoolSet = false;
	Map<Location, DyeColor > locations = new HashMap<Location, DyeColor>();
	ArrayList<Player> playerlist = new ArrayList<Player>();
	
	public void onStart(){
		final int amount = Config.color.size();
		round = 1;
		GetArenaLocation.getLocation(getMatch().getArena().getName(), Bukkit.getWorld(getMatch().getArena().getWorldGuardRegion().getWorldName()));
		Collection<ArenaPlayer> players = getMatch().getAlivePlayers();
		for (ArenaPlayer ap: players){
			playerlist.add(ap.getPlayer());
		}

		task1 = Bukkit.getScheduler().scheduleSyncRepeatingTask(WoolGame.getSelf(), new Runnable() {
			public void run() {
				if (WoolSet == true) {
					regenLayer();
					round++;
					WoolSet = false;
				}
					
				final int randomint = (int) Math.abs(Math.random()*amount);
				Collection<ArenaPlayer> players = getMatch().getAlivePlayers();
				for (ArenaPlayer ap: players){
					ItemStack item = new ItemStack(new Wool(Config.color.get(randomint)).toItemStack(1));
					for(int i = 0; i < 9; i++) {
						 ap.getPlayer().getInventory().setItem(i, item);
						 }
					 }
				task3 = Bukkit.getScheduler().scheduleSyncDelayedTask(WoolGame.getSelf(), new Runnable() {
					public void run() {
						DyeColor color = Config.color.get(randomint);
						setWool(color);
						Bukkit.getScheduler().cancelTask(task3);
					}
				}, (Math.round(Config.delayInTicks*0.7)-round*3)
				); 
					
				
				
			}
		
		
	}, 0L, Config.delayInTicks-round*3);
	}
	
	@Override
	public void onFinish(){
		Bukkit.getScheduler().cancelTask(task1);
		Bukkit.getScheduler().cancelTask(task3);
		task2 = Bukkit.getScheduler().scheduleSyncDelayedTask(WoolGame.getSelf(), new Runnable() {
            public void run() {
        		regenLayer();
        		Bukkit.getScheduler().cancelTask(task2);
               }
               }, 20L); 
	}
	
	//Entfernen der Wolle, die nicht der Farbe entsprechen
	public void setWool(DyeColor color) {
		for (int x = minX; x <= maxX; ++x) {
			for (int y = minY; y <= maxY; ++y) {
				for (int z = minZ; z <= maxZ; ++z) {
					Location loc = new Location(world, x, y, z);
					if(loc.getBlock().getType() == Material.WOOL){
						if(getWool(loc).getColor() != color){
						locations.put(loc.getBlock().getLocation(), getWool(loc).getColor());
						sendBlock(loc, 0, (byte) 0);
						WoolSet = true;
						}
			        }
					
				}
			}
		}
	}
	
	
	@SuppressWarnings("deprecation")
	public void sendBlock(Location loc, int blockId, byte data) {
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		World wo = loc.getWorld();
        net.minecraft.server.v1_10_R1.World w = ((CraftWorld) wo).getHandle();
        net.minecraft.server.v1_10_R1.Chunk chunk = w.getChunkAt(x >> 4, z >> 4);
        BlockPosition bp = new BlockPosition(x, y, z);
        int combined = blockId + (data << 12);
        IBlockData ibd = net.minecraft.server.v1_10_R1.Block.getByCombinedId(combined);
        chunk.a(bp, ibd);
        
        Chunk bukkitchunk = wo.getChunkAt(loc.getBlock());
        wo.refreshChunk(bukkitchunk.getX(), bukkitchunk.getZ());
    }

	//Wiederherstellen des Layers aus Wolle
	@SuppressWarnings("deprecation")
	public void regenLayer() {
		for(Map.Entry<Location, DyeColor> e : locations.entrySet()){
			  Location loc = e.getKey();
			  DyeColor color = e.getValue();
			  sendBlock(loc, 35, color.getData());
			}
	}
	
	public Wool getWool(Location loc) {
	  MaterialData state = loc.getBlock().getState().getData();
	  Wool wool = (Wool) state;
	  return wool;
	}
	
	//Respawn
	public void setPlayerDeath(Player p) {
		PlayerDeathEvent ede = new PlayerDeathEvent(p,new ArrayList<ItemStack>(),0, "");
        Bukkit.getPluginManager().callEvent(ede);
	}
	
	/* Fall-, Lava- oder Voidschaden f�hrt zum sofortigen Tot. Dabei wird ein Feuerwerk gespawnt */
	@ArenaEventHandler
	public void onEntityDamage(EntityDamageEvent Event) {
	    if(Event.getEntity() instanceof Player) {
	        Player player = (Player) Event.getEntity();
		    if(Event.getCause().equals(DamageCause.FALL)){
		    	player.getInventory().clear();
		    	firework(player);
		    	setPlayerDeath(player);
			}
		    if(Event.getCause().equals(DamageCause.VOID) || Event.getCause().equals(DamageCause.LAVA)) {
		    	setPlayerDeath(player);
		    }
	    }
	}
	
	public void firework(Player player) {
		Location location = player.getLocation();
    	final Firework firework = player.getWorld().spawn(location, Firework.class);
    	FireworkMeta data = (FireworkMeta) firework.getFireworkMeta();
    	data.addEffects(FireworkEffect.builder().trail(true).flicker(true).withColor(Color.RED).with(Type.BALL_LARGE).build());
    	data.setPower(0);
    	firework.setFireworkMeta(data);
	
    	task4 = Bukkit.getScheduler().scheduleSyncDelayedTask(WoolGame.getSelf(), new Runnable() {
    		public void run() {
    			firework.detonate();
    			Bukkit.getScheduler().cancelTask(task4);
    		}
    	}, 3L); 
	}
	
	@ArenaEventHandler
	public void onBlockPlace(BlockPlaceEvent Event) {
		Event.setCancelled(true);
  }
	
	@ArenaEventHandler
	public void onPlayerDrop(PlayerDropItemEvent Event) {
		Event.setCancelled(true);
  }
	
}
